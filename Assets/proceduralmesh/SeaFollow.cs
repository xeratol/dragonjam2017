﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaFollow : MonoBehaviour {

    public Transform target;
    public Vector3 snap = Vector3.one;

    void Start ()
    {
        Debug.Assert(target != null, "no target set", this);
    }
    
    void Update ()
    {
        if (target == null) return;

        var pos = target.position;
        pos.x = Mathf.Round(pos.x / snap.x) * snap.x;
        pos.y = Mathf.Round(pos.y / snap.y) * snap.y;
        pos.z = Mathf.Round(pos.z / snap.z) * snap.z;
        transform.position = pos;
    }
}
