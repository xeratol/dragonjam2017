﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(Grid))]
public class SeaWaves : MonoBehaviour {

    private Mesh mesh;
    private Grid grid;
    public float [] heightMultipier;
    public float [] xScale;
    public float [] yScale;
    private Vector2 gridScale;

    public float timedOffset;

    private Material material;

    void Start ()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        grid = GetComponent<Grid>();

        Debug.Assert(heightMultipier.Length == xScale.Length && xScale.Length == yScale.Length,
            "height and scales are not of equal lengths", this);

        gridScale = new Vector2(grid.xSize * grid.xScale, grid.ySize * grid.yScale);
        material = GetComponent<Renderer>().sharedMaterial;
    }
    
    void LateUpdate ()
    {
        var vertices = mesh.vertices;

        var offset = timedOffset * Time.time;
        for (var i = 0; i <vertices.Length; ++i)
        {
            var xCoord = (vertices[i].x + transform.position.x) / gridScale.x;
            var yCoord = (vertices[i].y + transform.position.z) / gridScale.y;
            var height = 0f;
            for (var j = 0; j < heightMultipier.Length; ++j)
            {
                height += Mathf.PerlinNoise(xCoord * xScale[j] + offset, yCoord * yScale[j] + offset) * heightMultipier[j];
                height -= heightMultipier[j] * 0.5f;
            }
            vertices[i].z = height;
        }

        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        material.mainTextureOffset = new Vector2(transform.position.x / (gridScale.x),
                                                 transform.position.z / (gridScale.y));
    }
}
