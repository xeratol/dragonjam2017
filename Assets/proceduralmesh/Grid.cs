﻿// http://catlikecoding.com/unity/tutorials/procedural-grid/

using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Grid : MonoBehaviour {

    public int xSize = 8, ySize = 8;
    public float xScale = 1;
    public float yScale = 1;

    private Mesh mesh;
    private Vector3[] vertices;

    private void Awake () {
        Generate();
        transform.localScale = Vector3.one;
    }

    private void Generate () {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "grid";

        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        var uv = new Vector2[vertices.Length];
        var tangents = new Vector4[vertices.Length];
        var tangent = new Vector4(1f, 0f, 0f, -1f);
        for (int i = 0, y = 0; y <= ySize; y++) {
            for (int x = 0; x <= xSize; x++, i++) {
                vertices[i] = new Vector3((x - xSize * 0.5f) * xScale,
                                          (y - ySize * 0.5f) * yScale);
                uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
                tangents[i] = tangent;
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.tangents = tangents;

        var triangles = new int[xSize * ySize * 6];
        for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++) {
            for (int x = 0; x < xSize; x++, ti += 6, vi++) {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
            }
        }
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }
}