﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConstantForce))]
public class MoveToPlayerSide : MonoBehaviour {

    private Transform player;
    private ConstantForce force;

    public float distance = 30;
    private Vector3 target;
    public float retargetCooldown = 2;
    private float cooldown = 0;
    public float turningTorque = 200;

    void Start ()
    {
        if (PlayerShip.instance != null)
        {
            player = PlayerShip.instance.transform;
        }
        force = GetComponent<ConstantForce>();
        AcquireTarget();
    }

    void AcquireTarget()
    {
        if (player == null) return;

        var dist = Random.Range(0.8f, 1.2f) * distance;
        var playerLeft = player.TransformPoint(Vector3.left * dist);
        var leftDistSq = (transform.position - playerLeft).sqrMagnitude;
        var playerRight = player.TransformPoint(Vector3.right * dist);
        var rightDistSq = (transform.position - playerRight).sqrMagnitude;

        target = (leftDistSq < rightDistSq) ? playerLeft : playerRight;
    }

    void TurnToTarget()
    {
        var targetDir = target - transform.position;
        var angle = Vector3.Angle(transform.right, targetDir);
        if (angle > 92)
        {
            force.relativeTorque = new Vector3(0, -turningTorque, 0);
        }
        else if (angle < 88)
        {
            force.relativeTorque = new Vector3(0, turningTorque, 0);
        }
        else
        {
            force.relativeTorque = Vector3.zero;
        }
    }
    
    void Update ()
    {
        cooldown += Time.deltaTime;
        if (cooldown > retargetCooldown)
        {
            AcquireTarget();
            cooldown = 0;
        }
        TurnToTarget();
    }

    private void OnDrawGizmosSelected()
    {
        if (player == null) return;

        var playerLeft = player.TransformPoint(Vector3.left * distance);
        var playerRight = player.TransformPoint(Vector3.right * distance);

        Gizmos.DrawLine(transform.position, playerLeft);
        Gizmos.DrawLine(transform.position, playerRight);
    }
}
