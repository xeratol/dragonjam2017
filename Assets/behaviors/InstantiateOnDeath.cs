﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateOnDeath : MonoBehaviour {

    public GameObject go;

    private void OnDeath()
    {
        if (go)
        {
            Instantiate(go, transform.position, transform.rotation);
        }
    }
}
