﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowBehavior : MonoBehaviour {

    public Transform target;
    private Vector3 relPos;
    private Vector3 smoothVel;
    public float smoothTime = 0.2f;

    private void Awake()
    {
        Debug.Assert(target != null, "target not set", this);
    }

    void Start ()
    {
        relPos = target.InverseTransformPoint(transform.position - target.position);
    }
    
    void LateUpdate ()
    {
        if (target == null)
        {
            return;
        }
        var targetPos = target.TransformPoint(relPos);
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref smoothVel, smoothTime);
        transform.LookAt(target);
    }
}
