﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFXOnCollision : MonoBehaviour {

    public AudioClip clip;
    public float volume = 1.0f;

    private void OnCollisionEnter()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position, volume);
    }
}
