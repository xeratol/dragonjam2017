﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonRangeBehavior : MonoBehaviour {

    public GameObject canon;

    private void Awake()
    {
        Debug.Assert(canon != null, "canon is not set", this);
    }

    private void OnTriggerEnter(Collider other)
    {
        canon.SendMessage("OnTargetEnteredRange", other.transform);
    }

    private void OnTriggerExit(Collider other)
    {
        canon.SendMessage("OnTargetExitedRange", other.transform);
    }
}
