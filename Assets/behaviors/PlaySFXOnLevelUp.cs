﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySFXOnLevelUp : MonoBehaviour {

    public AudioClip[] clips;
    private AudioSource audioSrc;
    public float volume = 1.0f;

    private void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }

    void OnLevelUp ()
    {
        audioSrc.PlayOneShot(clips[Random.Range(0, clips.Length)], volume);
    }
}
