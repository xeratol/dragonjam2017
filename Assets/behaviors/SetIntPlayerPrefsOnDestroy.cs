﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetIntPlayerPrefsOnDestroy : MonoBehaviour {

    public string PlayerPrefsKey;
    public int value;
    private bool isQuitting = false;

    void OnDestroy()
    {
        if (isQuitting) return;
        PlayerPrefs.SetInt(PlayerPrefsKey, value);
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }
}
