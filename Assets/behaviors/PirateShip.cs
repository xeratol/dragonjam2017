﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateShip : MonoBehaviour {

    void OnDeath ()
    {
        if (LevelManager.instance)
        {
            LevelManager.instance.PirateShipDied();
        }
    }
}
