﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnrescuedCrewBehavior : MonoBehaviour {

    public AudioClip clip;
    public GameObject sfxObject;
    public int numCrew = 1;

    private void OnTriggerEnter(Collider other)
    {
        ResourceManager.instance.Return(numCrew);

        var sfxObj = Instantiate(sfxObject, transform.position, transform.rotation);
        var sfxAudio = sfxObj.GetComponent<AudioSource>();
        sfxAudio.clip = clip;
        sfxAudio.Play();

        Destroy(gameObject);
    }
}
