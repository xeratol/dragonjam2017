﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ResourceConsumer))]
public class PlayerSpeedController : MonoBehaviour {

    private ConstantForce playerForce;
    public float[] forcePerLevel;
    private ResourceConsumer resource;

    void Start ()
    {
        playerForce = PlayerShip.instance.GetComponent<ConstantForce>();
        resource = GetComponent<ResourceConsumer>();

        Debug.Assert(forcePerLevel.Length == resource.maxLevel + 1, "force per level - max level mismatch", this);
    }
    
    void OnLevelUp()
    {
        playerForce.relativeForce = new Vector3(0, 0, forcePerLevel[resource.level]);
    }

    void OnLevelDown()
    {
        playerForce.relativeForce = new Vector3(0, 0, forcePerLevel[resource.level]);
    }

    void OnReturnAll()
    {
        playerForce.relativeForce = Vector3.zero;
    }
}
