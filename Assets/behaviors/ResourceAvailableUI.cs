﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceAvailableUI : MonoBehaviour {

    public Image[] indicators;
    public Color allocatedColor;
    public Color unallocatedColor;

    private int lastAvailableResource;

    void Start ()
    {
        Debug.Assert(indicators.Length == ResourceManager.instance.totalResource, "total resource - indicator mismatch", this);
        lastAvailableResource = ResourceManager.instance.GetAvailableResources();

        RefreshAll();
    }
    
    void Update ()
    {
        if (ResourceManager.instance == null)
        {
            return;
        }
        var availableResource = ResourceManager.instance.GetAvailableResources();
        if (lastAvailableResource == availableResource)
        {
            return;
        }

        lastAvailableResource = availableResource;
        RefreshAll();
    }

    void RefreshAll()
    {
        for (var i = 0; i < indicators.Length; ++i)
        {
            indicators[i].color = (i < lastAvailableResource) ? allocatedColor : unallocatedColor;
        }
    }
}
