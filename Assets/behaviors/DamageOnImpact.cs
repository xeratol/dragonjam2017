﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnImpact : MonoBehaviour {

    public float damage = 5;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject)
        {
            collision.gameObject.SendMessage("OnDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
    }
}
