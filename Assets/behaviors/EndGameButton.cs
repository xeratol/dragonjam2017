﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameButton : MonoBehaviour {

    public int levelIndex = 0;

    public void EndGame()
    {
        SceneManager.LoadScene(levelIndex);
    }
    
}
