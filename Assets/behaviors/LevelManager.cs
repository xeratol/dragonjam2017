﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public GameObject[] pirates;
    private int pirateCount = 0;
    public float spawnDist = 100;

    private Transform player;

    private delegate void LaunchWave();
    List<LaunchWave> waves;

    public GameObject victoryPanel;

    public static LevelManager instance { get; private set; }

    private void Awake()
    {
        Debug.Assert(instance == null, "only one level manager may exist", this);
        instance = this;
    }

    IEnumerator Start ()
    {
        player = PlayerShip.instance.transform;
        waves = new List<LaunchWave>();

        waves.Add( new LaunchWave(Wave01) );
        //waves.Add( new LaunchWave(Wave02) );
        waves.Add( new LaunchWave(Wave03) );
        //waves.Add( new LaunchWave(Wave04) );
        waves.Add( new LaunchWave(Wave05) );
        waves.Add( new LaunchWave(Wave06) );

        foreach (var wave in waves)
        {
            wave();
            while (pirateCount > 0)
            {
                yield return null;
            }
        }

        victoryPanel.SetActive(true);
    }
    
    void Wave01()
    {
        Spawn(pirates[0]);
    }

    void Wave02()
    {
        Spawn(pirates[0]);
        Spawn(pirates[0]);
    }

    void Wave03()
    {
        Spawn(pirates[0]);
        Spawn(pirates[0]);
        Spawn(pirates[1]);
    }

    void Wave04()
    {
        Spawn(pirates[1]);
        Spawn(pirates[1]);
    }

    void Wave05()
    {
        Spawn(pirates[0]);
        Spawn(pirates[0]);
        Spawn(pirates[0]);
        Spawn(pirates[0]);
        Spawn(pirates[0]);
        Spawn(pirates[0]);
    }

    void Wave06()
    {
        Spawn(pirates[2]);
    }

    GameObject Spawn(GameObject go)
    {
        ++pirateCount;
        var angleRad = Random.Range(0, Mathf.PI * 2);
        var pos = ((player == null) ? Vector3.zero : player.position)
            + new Vector3(Mathf.Cos(angleRad), 0, Mathf.Sin(angleRad)) * spawnDist;
        var rot = Quaternion.AngleAxis(270 -angleRad * Mathf.Rad2Deg, Vector3.up);
        return Instantiate(go, pos, rot);
    }

    private void OnDrawGizmosSelected()
    {
        if (player == null) return;
        Gizmos.DrawWireSphere(player.position, spawnDist);
    }

    public void PirateShipDied()
    {
        --pirateCount;
    }
}
