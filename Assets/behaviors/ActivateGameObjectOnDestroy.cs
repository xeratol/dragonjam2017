﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActivateGameObjectOnDestroy : MonoBehaviour {

    public GameObject go;
    private bool isQuitting = false;

    private void Start()
    {
        SceneManager.sceneUnloaded += OnSceneUnload;
    }

    private void OnDestroy()
    {
        if (go != null && !isQuitting)
        {
            go.SetActive(true);
        }
    }

    void OnSceneUnload(Scene s)
    {
        isQuitting = true;
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }
}
