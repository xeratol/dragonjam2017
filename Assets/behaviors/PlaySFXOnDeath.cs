﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFXOnDeath : MonoBehaviour {

    public AudioClip clip;
    public float volume = 1.0f;

    private void OnDeath()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position, volume);
    }
}
