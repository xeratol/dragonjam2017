﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

    public int totalResource = 6;
    public int startingResource = 6;
    private int resourceAvailable = 0;

    public static ResourceManager instance { get; private set; }

    void Awake ()
    {
        Debug.Assert(instance == null, "There can be no more than one ResourceManager", this);
        instance = this;

        resourceAvailable = startingResource;
    }

    public bool Has(int amount)
    {
        Debug.Assert(amount > 0);
        return (resourceAvailable >= amount);
    }

    public bool Consume(int amount)
    {
        Debug.Assert(amount > 0);
        var resourceLeft = resourceAvailable - amount;
        if (resourceLeft >= 0)
        {
            resourceAvailable = resourceLeft;
            return true;
        }
        return false;
    }

    public void Return(int amount)
    {
        Debug.Assert(amount >= 0);
        resourceAvailable += amount;
        if (resourceAvailable > totalResource)
        {
            resourceAvailable = totalResource;
        }
    }

    public int GetAvailableResources()
    {
        return resourceAvailable;
    }
}
