﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayerOnDestroy : MonoBehaviour {

    public float damage = 10;

    private void OnDestroy()
    {
        if (PlayerShip.instance)
        {
            PlayerShip.instance.SendMessage("OnDamage", damage, SendMessageOptions.DontRequireReceiver);
        }
    }
}
