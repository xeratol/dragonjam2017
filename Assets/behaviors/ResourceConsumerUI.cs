﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ResourceConsumer))]
public class ResourceConsumerUI : MonoBehaviour {

    public KeyCode key;
    public KeyCode keyModifierToDecrease = KeyCode.None;
    public KeyCode keyToDecrease = KeyCode.None;
    private static KeyCode keyToReset = KeyCode.Alpha0;

    public Image [] levelIndicators;
    public Color allocatedColor;
    public Color unallocatedColor;

    private ResourceConsumer resourceConsumer;

    void Start ()
    {
        resourceConsumer = GetComponent<ResourceConsumer>();
        Debug.Assert(levelIndicators.Length == resourceConsumer.maxLevel, "Indicator-Max Level mismatch", this);
        UpdateIndicators();
    }
    
    void Update ()
    {
        if (Input.GetKeyDown(key))
        {
            if (keyModifierToDecrease != KeyCode.None && Input.GetKey(keyModifierToDecrease))
            {
                if (resourceConsumer.LevelDown())
                {
                    levelIndicators[resourceConsumer.level].color = unallocatedColor;
                }
            }
            else
            {
                if (resourceConsumer.LevelUp())
                {
                    levelIndicators[resourceConsumer.level - 1].color = allocatedColor;
                }
            }
        }
        else if (keyToDecrease != KeyCode.None && Input.GetKeyDown(keyToDecrease))
        {
            if (resourceConsumer.LevelDown())
            {
                levelIndicators[resourceConsumer.level].color = unallocatedColor;
            }
        }
        else if (Input.GetKeyDown(keyToReset))
        {
            resourceConsumer.ReturnAll();
            UpdateIndicators();
        }
    }

    public void UpdateIndicators ()
    {
        for (var i = 0; i < resourceConsumer.maxLevel; ++i)
        {
            levelIndicators[i].color = (i < resourceConsumer.level) ? allocatedColor : unallocatedColor;
        }
    }
}
