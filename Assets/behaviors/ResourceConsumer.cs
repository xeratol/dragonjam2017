﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceConsumer : MonoBehaviour {

    public int maxLevel = 4;
    public int levelCost = 1;
    public int level { get; private set; }

    void Start ()
    {
        level = 0;
    }
    
    public bool LevelUp()
    {
        if (level >= maxLevel)
        {
            return false;
        }

        if (ResourceManager.instance.Consume(levelCost))
        {
            ++level;
            SendMessage("OnLevelUp", SendMessageOptions.DontRequireReceiver);
            return true;
        }
        return false;
    }

    public bool LevelDown()
    {
        if (level <= 0)
        {
            return false;
        }

        ResourceManager.instance.Return(levelCost);
        --level;
        SendMessage("OnLevelDown", SendMessageOptions.DontRequireReceiver);
        return true;
    }

    public void ReturnAll()
    {
        if (level > 0)
        {
            ResourceManager.instance.Return(level);
            level = 0;
            SendMessage("OnReturnAll", SendMessageOptions.DontRequireReceiver);
        }
    }
}
