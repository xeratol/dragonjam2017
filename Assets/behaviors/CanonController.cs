﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ResourceConsumer))]
public class CanonController : MonoBehaviour
{
    private ResourceConsumer resource;

    public Transform canon;
    public Transform canonBall;
    public Vector3 localLaunchDirection;
    public Vector3 localLaunchCenter;
    public float [] cooldown;
    private float readiness = 1;
    public float [] turnSpeed; // degrees per sec
    public float explosivePower = 0.5f;

    private Transform target;
    private List<Transform> availableTargets;

    public Image cooldownTimer;

    void Awake ()
    {
        Debug.Assert(canon != null, "canon not set", this);
        Debug.Assert(canonBall != null, "canon ball not set", this);
        availableTargets = new List<Transform>();

        resource = GetComponent<ResourceConsumer>();
        Debug.Assert(cooldown.Length == resource.maxLevel + 1, "cooldown - max level mismatch", this);
        Debug.Assert(turnSpeed.Length == resource.maxLevel + 1, "turn speed - max level mismatch", this);
    }

    public void OnTargetEnteredRange(Transform newTarget)
    {
        availableTargets.Add(newTarget);
    }

    public void OnTargetExitedRange(Transform oldTarget)
    {
        availableTargets.Remove(oldTarget);
    }

    void CleanUpAvailableTargets()
    {
        availableTargets.RemoveAll(item => item == null);
    }
    
    void Update ()
    {
        UpdateTimer();
        CleanUpAvailableTargets();
        if (availableTargets.Count == 0 || resource.level == 0)
        {
            return;
        }

        if (target == null || availableTargets.IndexOf(target) < 0)
        {
            target = availableTargets[0];
        }

        Aim();
    }

    void Aim()
    {
        if (canon == null)
        {
            return;
        }
        var dirtoTarget = target.position - canon.position;
        dirtoTarget.y = 0;
        var currForward = -canon.right; // it's rotated
        currForward.y = 0;

        var angleBetween = Vector3.Angle(currForward, dirtoTarget);
        var angleInc = turnSpeed[resource.level] * Time.deltaTime;
        if (angleInc > angleBetween)
        {
            canon.Rotate(0, 0, -angleBetween);
            Fire();
        }
        else
        {
            if (Vector3.Cross(dirtoTarget, currForward).y < 0)
            {
                angleInc = -angleInc;
            }

            canon.Rotate(0, 0, -angleInc);
        }
    }

    void UpdateTimer()
    {
        if (resource.level > 0 && readiness < 1)
        {
            readiness += Time.deltaTime / cooldown[resource.level];
            cooldownTimer.fillAmount = 1 - readiness;
        }
    }

    void Fire()
    {
        if (readiness < 1)
        {
            return;
        }
        readiness = 0;

        var dirtoTarget = target.position - canon.position;
        //dirtoTarget.y = 0;
        var dist = dirtoTarget.magnitude;

        var center = canon.TransformPoint(localLaunchCenter);
        var dir = canon.TransformDirection(localLaunchDirection);

        var ball = Instantiate(canonBall, center, Quaternion.identity) as Transform;
        ball.GetComponent<Rigidbody>().AddForce(dir * dist * explosivePower, ForceMode.Impulse);

        SendMessage("OnCannonFire", SendMessageOptions.DontRequireReceiver);
    }

    private void OnDrawGizmosSelected()
    {
        if (canon == null) return;

        var center = canon.TransformPoint(localLaunchCenter);
        var dir = canon.TransformDirection(localLaunchDirection);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(center, 0.5f);
        Gizmos.DrawRay(center, dir * 5);

        Gizmos.color = Color.red;
        if (target != null)
        {
            var pos = canon.position;
            pos.y = 0;
            Gizmos.DrawLine(pos, target.position);
        }
    }
}
