﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpUIBehavior : MonoBehaviour {

    public static HelpUIBehavior instance { get; private set; }
    private Text textUI;

    void Awake ()
    {
        Debug.Assert(instance == null, "only 1 instance allowed", this);
        instance = this;

        textUI = GetComponentInChildren<Text>();
        Debug.Assert(textUI != null, "No text UI found in children", this);
    }
    
    public void UpdateText (string text)
    {
        textUI.text = text;
    }
}
