﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnrescuedCrewUIBehavior : MonoBehaviour {

    private Transform originalParent;
    public Vector2 screenOffset;

    void Start ()
    {
        originalParent = transform.parent;
        var canvas = GameObject.FindObjectOfType<Canvas>();
        transform.SetParent(canvas.transform);
    }
    
    void LateUpdate ()
    {
        if (originalParent == null)
        {
            Destroy(gameObject);
            return;
        }

        var newPos = RectTransformUtility.WorldToScreenPoint(Camera.main, originalParent.position) + screenOffset;
        transform.position = newPos;
        transform.rotation = Quaternion.identity;
    }
}
