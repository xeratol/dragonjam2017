﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanelActivator : MonoBehaviour {

    public KeyCode key = KeyCode.Escape;
    public GameObject pausePanel;

    void Update ()
    {
        if (Input.GetKeyUp(key))
        {
            pausePanel.SetActive(!pausePanel.activeSelf);
        }
    }
}
