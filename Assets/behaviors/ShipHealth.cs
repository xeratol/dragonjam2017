﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipHealth : MonoBehaviour {

    public float maxHealth = 20;
    public float health { get; private set; }
    public Image ui;

    void Start ()
    {
        health = maxHealth;
    }
    
    void OnDamage (float damage)
    {
        health -= damage;
        UpdateUI();
        if (health <= 0)
        {
            SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
    }

    void OnRepair (float amount)
    {
        health += amount;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        UpdateUI();
    }

    void UpdateUI()
    {
        if (ui != null)
        {
            ui.fillAmount = health / maxHealth;
        }
    }
}
