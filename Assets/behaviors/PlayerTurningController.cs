﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ResourceConsumer))]
public class PlayerTurningController : MonoBehaviour {

    private ConstantForce playerForce;
    public float[] torquePerLevel;
    private ResourceConsumer resource;
    private ResourceConsumerUI resourceUI;

    public KeyCode keyToSteerLeft = KeyCode.LeftArrow;
    public KeyCode keyToSteerRight = KeyCode.RightArrow;

    private enum Direction
    {
        Left, None, Right
    }
    private Direction currentHeading = Direction.None;

    void Start ()
    {
        playerForce = PlayerShip.instance.GetComponent<ConstantForce>();
        resource = GetComponent<ResourceConsumer>();
        resourceUI = GetComponent<ResourceConsumerUI>();

        Debug.Assert(torquePerLevel.Length == resource.maxLevel + 1, "torque per level - max level mismatch", this);
    }

    void Update ()
    {
        var torque = Vector3.zero;
        if (Input.GetKeyDown(keyToSteerLeft))
        {
            if (currentHeading == Direction.Right)
            {
                resource.LevelDown();
                if (resource.level == 0)
                {
                    currentHeading = Direction.None;
                }
                else
                {
                    torque.y = torquePerLevel[resource.level];
                }
                playerForce.torque = torque;
            }
            else if (resource.LevelUp())
            {
                currentHeading = Direction.Left;
                torque.y = -torquePerLevel[resource.level];
                playerForce.torque = torque;
            }
            resourceUI.UpdateIndicators();
        }
        else if (Input.GetKeyDown(keyToSteerRight))
        {
            if (currentHeading == Direction.Left)
            {
                resource.LevelDown();
                if (resource.level == 0)
                {
                    currentHeading = Direction.None;
                }
                else
                {
                    torque.y = -torquePerLevel[resource.level];
                }
                playerForce.torque = torque;
            }
            else if (resource.LevelUp())
            {
                currentHeading = Direction.Right;
                torque.y = torquePerLevel[resource.level];
                playerForce.torque = torque;
            }
            resourceUI.UpdateIndicators();
        }
    }

    void OnReturnAll()
    {
        currentHeading = Direction.None;
        playerForce.torque = Vector3.zero;
    }
}
