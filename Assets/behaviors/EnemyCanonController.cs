﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCanonController : MonoBehaviour {

    public Transform canonBall;
    public Vector3 localLaunchDirection;
    public Vector3 localLaunchCenter;
    public float cooldown;
    private float readiness = 1;
    public float turnSpeed; // degrees per sec
    public float explosivePower = 0.5f;

    private Transform target;
    private List<Transform> availableTargets;

    void Awake ()
    {
        Debug.Assert(canonBall != null, "canon ball not set", this);
        availableTargets = new List<Transform>();
    }

    public void OnTargetEnteredRange(Transform newTarget)
    {
        availableTargets.Add(newTarget);
    }

    public void OnTargetExitedRange(Transform oldTarget)
    {
        availableTargets.Remove(oldTarget);
    }

    void CleanUpAvailableTargets()
    {
        availableTargets.RemoveAll(item => item == null);
    }

    void UpdateTimer()
    {
        if (readiness < 1)
        {
            readiness += Time.deltaTime / cooldown;
        }
    }

    void Fire()
    {
        if (readiness < 1)
        {
            return;
        }
        readiness = 0;

        var dirtoTarget = target.position - transform.position;
        //dirtoTarget.y = 0;
        var dist = dirtoTarget.magnitude;

        var center = transform.TransformPoint(localLaunchCenter);
        var dir = transform.TransformDirection(localLaunchDirection);

        var ball = Instantiate(canonBall, center, Quaternion.identity) as Transform;
        ball.GetComponent<Rigidbody>().AddForce(dir * dist * explosivePower, ForceMode.Impulse);

        SendMessage("OnCannonFire", SendMessageOptions.DontRequireReceiver);
    }

    void Update()
    {
        UpdateTimer();
        CleanUpAvailableTargets();
        if (availableTargets.Count == 0)
        {
            return;
        }

        if (target == null || availableTargets.IndexOf(target) < 0)
        {
            target = availableTargets[0];
        }

        Aim();
    }

    void Aim()
    {
        var dirtoTarget = target.position - transform.position;
        dirtoTarget.y = 0;
        var currForward = -transform.right; // it's rotated
        currForward.y = 0;

        var angleBetween = Vector3.Angle(currForward, dirtoTarget);
        var angleInc = turnSpeed * Time.deltaTime;
        if (angleInc > angleBetween)
        {
            transform.Rotate(0, 0, -angleBetween);
            Fire();
        }
        else
        {
            if (Vector3.Cross(dirtoTarget, currForward).y < 0)
            {
                angleInc = -angleInc;
            }

            transform.Rotate(0, 0, -angleInc);
        }
    }

    private void OnDrawGizmosSelected()
    {
        var center = transform.TransformPoint(localLaunchCenter);
        var dir = transform.TransformDirection(localLaunchDirection);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(center, 0.5f);
        Gizmos.DrawRay(center, dir * 5);

        Gizmos.color = Color.red;
        if (target != null)
        {
            var pos = transform.position;
            pos.y = 0;
            Gizmos.DrawLine(pos, target.position);
        }
    }
}
