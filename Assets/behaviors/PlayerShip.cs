﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour {

    public static PlayerShip instance { get; private set; }

    public float repairRate = 1;

    void Awake ()
    {
        Debug.Assert(instance == null, "there can only be one player ship", this);
        instance = this;
    }

    private void Update()
    {
        if (ResourceManager.instance)
        {
            var unassignedCrew = ResourceManager.instance.GetAvailableResources();
            if (unassignedCrew > 0)
            {
                SendMessage("OnRepair", unassignedCrew * repairRate * Time.deltaTime);
            }
        }
    }
}
