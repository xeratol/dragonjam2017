﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DestroyAfterSFXPlays : MonoBehaviour {

    private AudioSource audioSrc;

    void Start ()
    {
        audioSrc = GetComponent<AudioSource>();
    }
    
    void LateUpdate ()
    {
        if (!audioSrc.isPlaying)
        {
            Destroy(gameObject);
        }
    }
}
