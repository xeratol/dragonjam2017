﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateHelpTextOnDestroy : MonoBehaviour {

    public string newText;
    private bool isQuitting = false;

    void OnDestroy ()
    {
        if (HelpUIBehavior.instance != null && !isQuitting)
        {
            HelpUIBehavior.instance.UpdateText(newText.Replace("\\n", "\n"));
        }
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }
}
