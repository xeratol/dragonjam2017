﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonBallBehavior : MonoBehaviour {

    public float yToAutodestruct = -1;

    private void OnCollisionEnter()
    {
        Destroy(gameObject);
    }

    private void Update()
    {
        if (transform.position.y < yToAutodestruct)
        {
            Destroy(gameObject);
        }
    }
}
