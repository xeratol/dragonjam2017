﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenBehavior : MonoBehaviour {

    public GameObject playButton;

    void Start ()
    {
        var tutorialDone = (PlayerPrefs.GetInt("TUTORIAL_DONE", 0) != 0);
        playButton.SetActive(tutorialDone);
    }

    public void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
